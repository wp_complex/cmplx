<?php while (have_posts()) : the_post(); ?>
    <section class="main-panel">
        <div <?php hybrid_attr('content'); ?>>
            <h1 <?php echo hybrid_get_attr('entry-title'); ?>>
                <?php the_title(); ?>
            </h1>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div>
    </section>
<?php endwhile; ?>