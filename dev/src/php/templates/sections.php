<?php
$section_title = get_sub_field ('title');
$section_id = sonderzeichen(str_replace(' ', '_', strtolower($section_title)));

$align = get_sub_field('align');
$size = get_sub_field('size');
$style = get_sub_field('style');
$height = get_sub_field('height');
//$child_attribute = get_sub_field('child_attribute');
//$child_custome_classes = get_sub_field('custome_classes_child');
$child_classes = " ";


//$parent_attribute = get_sub_field('parent_attribute');
//$parent_custome_classes = get_sub_field('custome_classes_parent');
$parent_classes = " ". $height . " " . $style;
?>


        <?php if ($align) $child_classes = $child_classes . " text_" . $align; ?>
        <?php if ($size) $child_classes = $child_classes . " " . $size . "_box"; ?>
        <?php if ($style) $child_classes = $child_classes . " " . $style . "_scheme"; ?>
            

            <?php if (have_rows('content')) : ?>
                <?php while (have_rows('content')) : the_row(); ?>
                    <?php if (get_row_layout()) : ?>
                        <?php $id = sonderzeichen(get_row_layout()); ?>
                            <section class="main <?php echo $parent_classes; ?>"  id="<?php echo $section_id; ?>" >
                           
                                <?php include(locate_template('wp_setup/components/sections/' . get_row_layout() . '.php')); ?>
                            </section>
                    <?php endif; ?>  
                <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?> <!-- have_rows('content')  -->
