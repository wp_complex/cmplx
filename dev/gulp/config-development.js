var fs            = require('fs');
//var packageConfig = require(process.env.PWD + '/package.json');

var root = '../';
var dest = '../_'; // + packageConfig.name
var src = './src';

var bower_components = './src/bower_components';
var node_modules = './node_modules';

module.exports = {
  options: {
    version: "1.0"
  },
  autoprefixer: [
    'last 2 version',
    'safari 5',
    'ie 9',
    'opera 12.1',
    'ios 6',
    'android 4'
  ],
  browserSync: {
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: [dest]
    },
    open: false,
    files: [
      dest + "/**",
      // Exclude Map files
      "!" + dest + "/**.map"
    ]
  },
  images: {
    src: src + "/resources/img/**",
    dest: dest + "/img"
  },

  css: {
    src: src + "/resources/scss/**", // files which are watched for changes, but not compiled directly
    main: src + "/resources/scss/*.{scss}", // files which are compiled with all their decendants
    watch: src + '/resources/scss/**/*',
    dest: root,
    sassOpts: {
      outputStyle: 'nested',
      imagePath: dest + "/resources/images",
      precision: 3,
      errLogToConsole: true
    },
    processors: [
        require('postcss-assets')({
            loadPaths: ['images/'],
            basePath: dest,
            baseUrl: '/wp-content/themes/gulp-wp/'
        }),
        require('autoprefixer')({
            browsers: ['last 2 versions', '> 2%']
        }),
        require('css-mqpacker'),
        require('cssnano')
    ]
  },
  svg: {
    src: src + "/resources/svg/**",
    dest: dest + "/resources/svg"
  },
   index: {
    src: src + '/php/index/**/*.php',
    dest:  root
  },
  markup: {
    src: src + '/php/**/*.php',
    dest: dest + "/php"
  },

  copy: {
    src: [
      src + '/*.*' // Meta files e.g. Screenshot for WordPress Theme Selector
    ],
    dest: root,
    options: {
      base: src // ensure that all copy tasks keep folder structure
    }
  },
  bump: {
    unreleasedPlaceholder: /## unreleased/ig, // To be replaced in documents with version number
    prereleaseChangelogs: false, // If true, changelog update with prerelease bump
    options: {
      preid : 'beta' // Set the prerelase tag to use
    }
  },
  changelog: {
    src: './CHANGELOG.md',
    dest: dest
  },
  browserify: {
    // Enable source maps
    debug: true,
    transforms: {
      uglifyify: false
    },
    // Additional file extentions to make optional
    extensions: ['.coffee', '.hbs'],
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/resources/js/index.js',
      dest:  dest +"/js",
      outputName: 'app.js'
    }]
  }
};
