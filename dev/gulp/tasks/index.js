'use strict';
var replace       = require('gulp-replace');

module.exports = function(gulp, config){
  gulp.task('index', function() {
    return gulp.src(config.index.src)
    .pipe(replace(/{PKG_VERSION}/g, config.options.version))
    .pipe(gulp.dest(config.index.dest));
  });
};
