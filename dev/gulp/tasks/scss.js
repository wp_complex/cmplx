"use strict";

const
  gutil = require("gulp-util"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  reload = require("browser-sync").reload,
  handleErrors = require("../util/handleErrors");

// Browser-sync
var browsersync = false;

module.exports = function(gulp, config) {
  gulp.task("css", () => {
    return gulp
      .src(config.css.src)
      .pipe(sass(config.css.sassOpts))
      .pipe(postcss(config.css.processors))
      .pipe(gulp.dest(config.css.dest))
      .pipe(
        browsersync
          ? browsersync.reload({
              stream: true
            })
          : gutil.noop()
      )
      .on("error", handleErrors);
  });
};
