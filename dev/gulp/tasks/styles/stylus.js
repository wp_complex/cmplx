'use strict';

var plumber       = require('gulp-plumber');
var autoprefixer  = require('gulp-autoprefixer');
var stylus        = require('gulp-stylus');
var replace       = require('gulp-replace');
var reload        = require('browser-sync').reload;
var handleErrors  = require('../../util/handleErrors');

module.exports = function(gulp, config){

  gulp.task('css', () => {
      return gulp.src(config.css.src)
          .pipe(sass(config.css.sassOpts))
          .pipe(postcss(config.css.processors))
          .pipe(gulp.dest(config.css.dest))
          .pipe(browsersync ? browsersync.reload({
              stream: true
          }) : gutil.noop()) .on('error', handleErrors);
  });

};
