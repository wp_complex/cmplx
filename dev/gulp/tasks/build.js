'use strict';

module.exports = function(gulp){
  gulp.task('build', ['browserify', 'css', 'images', 'svg',  'index','markup', 'copy', 'changelog']);
};
